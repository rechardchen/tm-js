var Controller = require('tm-js').Controller;

var c = new Controller({
            host: 'localhost',
            port: 8000
        });
c.on('connect',function(){
    c.emit('mail','too hungry','i need to have lunch now');
    c.on('feed',function(feed){
        console.log(feed);
    });
    setTimeout(function(){
        c.disconnect();
    },3000);
});
