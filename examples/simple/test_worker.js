var tm = require('tm-js');

var server = new tm.Worker('localhost');
server.on("connection",function(socket){
    console.log(socket.id,'has connected');
    socket.on('mail',function(title,content){
        console.log(new Date+'('+socket.id+')',':','['+title+']['+content+']');
        socket.broadcast("feed",'['+title+'] from '+socket.id);
    });
    socket.on('disconnect',function(){
        console.log(socket.id,'disconnect');
    });
});
