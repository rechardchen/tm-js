(function(exports){
    var io;

    if('object' == typeof module && 'function' == typeof require)
        io = require('socket.io-client');
    else
        io = exports.io;   //require socket.io-client in browser

    function EventEmitter(){  //TODO: use sea.js to manage depencies
        this._events = {};
    } 

    EventEmitter.prototype.emit = function(type){
        if('string' == typeof type && this._events.hasOwnProperty(type)){
            var args = Array.prototype.slice.call(arguments,1);
            for(var i = 0,l = this._events[type].length; i < l;++i)
                this._events[type][i].apply(this,args);
        } 
    }
    EventEmitter.prototype.addListener = function(type,listener){
        if('string' == typeof type && 'function' == typeof listener){
            if(Array.isArray(this._events[type]))
                this._events[type].push(listener);
            else
                this._events[type] = [listener];
        }
    }
    EventEmitter.prototype.removeListener = function(type,listener){
        var i = 0;
        if('string' == typeof type && this._events.hasOwnProperty(type) && 
                'function' == typeof listener){
            while(i < this._events[type].length){
                if(this._events[type][i] == listener)
                   this._events[type].splice(i,1);
                else 
                   i += 1; 
            } 
        }
    }
    EventEmitter.prototype.removeAllListeners = function(type){
        if('string' == typeof type) delete this._events[type];
    }
    EventEmitter.prototype.on = EventEmitter.prototype.addListener;

    exports.Controller = function(hostDetail,option){
        var host = hostDetail.host || 'localhost',
            port = 'number' == typeof hostDetail.port? hostDetail.port: 80,
            self = this,
            event = new EventEmitter,
            connected = false,
            socket = io.connect('http://'+host+':'+port,{
                "force new connection": true
               ,"reconnect": false
            });

        Object.defineProperty(this,'connected',{
            get: function(){
                return connected;
            }
        });

        socket.on('connect',function(){
            socket.emit('select-worker','object' == typeof option? option: {}); 
        });
        socket.on('disconnect',function(){
            connected = false;
            event.emit('disconnect');
        });
        socket.on('connection-establish',function(){
            connected = true;
            event.emit("connect");
        });
        socket.on('tm-data',function(data){
            var command = data.command, data = data.data; //data is an array
            if(data){
                data.unshift(command);
                event.emit.apply(event,data); 
            }else
                event.emit(command);
        });
        this.emit = function(command){
            if(connected && 'string' == typeof command && 
                    command != 'connect' && command != 'disconnect')
                socket.emit('tm-data',{
                                command: command,
                                data: Array.prototype.slice.call(arguments,1)
                            });
        }
        this.on = function(command,listener){
            if('function' == typeof listener && 'string' == typeof command)
                event.on(command,listener);
        }
        this.disconnect = function(){
            socket.disconnect();
        }
    };
})(typeof module == 'object'? module.exports: window);
