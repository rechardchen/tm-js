var sio = require('socket.io'),
    uuid = require('node-uuid'),
    util = require('util'),
    EventEmitter = require('events').EventEmitter,
    WORKER_PORT = 8511;

function setConstant(obj,name,value){
    Object.defineProperty(obj,name,{
        get: function(){
            return value;
        }
    });
}

function listen(app_or_port){    //create a gateway
    var workerServer = sio.listen(WORKER_PORT),
        controllerServer = sio.listen(app_or_port),
        connections = {},
        controllers = {},
        workers = {};
    
    //reduce logging
    controllerServer.set('log level',1);
    workerServer.set('log level',1);
    //Connection object 
    function Connection(controller,worker){
        var self = this,event = new EventEmitter;
        this.id = uuid();
        this.controller = controller, this.controllerId = controller.id;
        this.worker = worker, this.workerId = worker.id;
        connections[self.id] = this;
        controllers[controller.id].connectionId = self.id;
        controller.emit("connection-establish");
        workers[worker.id].connections[self.id] = true;
        workers[worker.id].nConnections += 1;
        worker.emit("connection-establish",{
			connectionId: self.id
		});

        this.emit = function(type){
            event.emit.apply(event,Array.prototype.slice.apply(arguments));
        }

        this.on = function(type,listener){
            event.on(type,listener);
        }

        this.on("tm-data",function(data,toWorker){
            if(toWorker === true){
                worker.emit("tm-data",data,self.id);
            }else if(toWorker === false){
                controller.emit("tm-data",data);
            }
        });

        this.delete = function(){
            if(connections.hasOwnProperty(self.id)){
                if(controllers.hasOwnProperty(self.controllerId)){
                    controller.disconnect();
                }
                if(workers.hasOwnProperty(self.workerId) && 
                    workers[self.workerId].connections.hasOwnProperty(self.id)){
                    worker.emit("connection-disconnect",self.id);
                    delete workers[self.workerId].connections[self.id];
                    workers[self.workerId].nConnections -= 1;
                }
                delete connections[self.id];
            }
        };
    }
    
    controllerServer.sockets.on("connection",function(socket){
        controllers[socket.id] = {
            conn: socket
           ,connectionId: null
        };
        socket.on('select-worker',function(selector){
            var worker, _worker,connection;
            if(typeof selector == 'object'){
                outer:
                for(var w in workers){
                    _worker = workers[w]; 
                    for(var k in selector)
                        if(_worker.profile[k] != selector[k])
                            continue outer;
                    if(worker == undefined ||
                        _worker.nConnections < worker.nConnections)
                        worker = _worker;
                }
            }
             
            if(worker)
                connection = new Connection(socket,worker.conn);
            else
                socket.disconnect();
        }); 
        
        socket.on('disconnect',function(){
            if(controllers.hasOwnProperty(socket.id)){
                var controller = controllers[socket.id];
                delete controllers[socket.id];
                if(controller.connectionId && connections.hasOwnProperty(controller.connectionId)) {
                    var connection = connections[controller.connectionId];
                    connection.delete();
                }
            }
        });
        
        socket.on('tm-data',function(data){  //redirect data
            var controller = controllers[socket.id],
                connection;
            if(controller.connectionId && connections.hasOwnProperty(controller.connectionId)){
                connection = connections[controller.connectionId];
                connection.emit("tm-data",data,true);
            }
        });
    });

    workerServer.sockets.on("connection",function(socket){
        workers[socket.id] = {
            conn: socket
           ,profile: {}
           ,connections: {}
           ,nConnections: 0
        };

        socket.on('update-profile',function(profile){
            workers[socket.id].profile = profile;
        });
        
        socket.on('disconnect',function(){
            if(workers.hasOwnProperty(socket.id)){
                var worker = workers[socket.id];
                delete workers[socket.id];
                for(var id in worker.connections){
                    var connection = connections[id];
                    connection.delete();
                }
            }
        });

        socket.on('tm-data',function(data,connectionId){
            if(connections.hasOwnProperty(connectionId)){
                var connection = connections[connectionId];
                if (connection.workerId == socket.id) 
                    connection.emit("tm-data",data,false);
            }
        });

        socket.on('close-connection',function(connectionId){
            var connection = connections[connectionId];
            connection.delete();
        });
    });
}

exports.listen = listen;
setConstant(exports,"WORKER_PORT",WORKER_PORT);
