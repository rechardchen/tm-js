(function(exports){
    if('object' == typeof module && 'function' == typeof require)
        var io = require('socket.io-client');
    function EventEmitter(){  //TODO: use sea.js to manage depencies
        this._events = {};
    } 

    EventEmitter.prototype.emit = function(type){
        if('string' == typeof type && this._events.hasOwnProperty(type)){
            var args = Array.prototype.slice.call(arguments,1);
            for(var i = 0,l = this._events[type].length; i < l;++i)
                this._events[type][i].apply(this,args);
        } 
    }
    EventEmitter.prototype.addListener = function(type,listener){
        if('string' == typeof type && 'function' == typeof listener){
            if(Array.isArray(this._events[type]))
                this._events[type].push(listener);
            else
                this._events[type] = [listener];
        }
    }
    EventEmitter.prototype.removeListener = function(type,listener){
        var i = 0;
        if('string' == typeof type && this._events.hasOwnProperty(type) && 
                'function' == typeof listener){
            while(i < this._events[type].length){
                if(this._events[type][i] == listener)
                   this._events[type].splice(i,1);
                else 
                   i += 1; 
            } 
        }
    }
    EventEmitter.prototype.removeAllListeners = function(type){
        if('string' == typeof type) delete this._events[type];
    }
    EventEmitter.prototype.on = EventEmitter.prototype.addListener;
    
    function Worker(host){
        var self = this, 
            event = new EventEmitter,
            profile = {},
            connections = {},
            socket = io.connect('http://'+host+':8511',{   //notice WORKER_PORT = 8511
                "force new connection": true
               ,"reconnect": false
            });
       
       //profile control 
       Object.defineProperty(this,'profile',{
            get: function(){
                return profile;
            }
        }); 
        this.updateProfile = function(){
            socket.emit("update-profile",profile);
        }
        function Connection(id){
            this.id = id;
            this.event = new EventEmitter;
        }

        Connection.prototype.emit = function(type){
            if('string' == typeof type){
                if(type != 'connect' && type != 'disconnect'){
                    socket.emit("tm-data",{
                        command: type,
                        data: Array.prototype.slice.call(arguments,1)
                    },this.id);
                }
            }
        };

        Connection.prototype.on = function(type,listener){
            this.event.on(type,listener);     
        }
        Connection.prototype.broadcast = function(type){
            if('string' == typeof type){
                var args = Array.prototype.slice.apply(arguments);
                for(var id in connections)
                    if(id != this.id){
                        var connection = connections[id];
                        connection.emit.apply(connection,args);
                    }
            } 
        }

        Connection.prototype.disconnect = function(){
            delete connections[this.id];
            socket.emit("close-connection",this.id);
            this.event.emit("disconnect");
        }
        

        socket.on("connection-establish",function(details){
            var connectionId = details.connectionId,
                connection = new Connection(connectionId);
            connections[connection.id] = connection;
            event.emit("connection",connection);
        });
        
        socket.on("tm-data",function(data,connectionId){
            var connection = connections[connectionId];
            if(data.data){
                var args = [data.command].concat(data.data);
                connection.event.emit.apply(connection.event,args);
            }else
                connection.event.emit(data.command);
        });

        socket.on("connection-disconnect",function(id){
            var connection = connections[id];
            connection.event.emit('disconnect');
            delete connections[id];
        });
        
        socket.on("connect",function(){
            event.emit('connect');
        });
        socket.on("disconnect",function(){
            connections = {};
            //console.log('WARNING: disconnected from websocket server');
            //process.exit(-1);
            event.emit('disconnect');
        });

        this.broadcast = function(type){   //broadcast
            if('string' == typeof type){
                var args = Array.prototype.slice.apply(arguments);
                for(var id in connections){
                    var connection = connections[id];
                    connection.emit.apply(connection,args);
                }
            }
        } 
        this.on = function(type,listener){
            if('string' == typeof type && 'function' == typeof listener){
                event.on(type,listener); 
            }
        }
    };
    exports.Worker = Worker;
})('object' == typeof module ? module.exports: window);
